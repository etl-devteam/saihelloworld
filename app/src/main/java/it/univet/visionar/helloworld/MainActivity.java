package it.univet.visionar.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import it.univet.visionar.visionarservice.VisionARService;

public class MainActivity extends AppCompatActivity {

    VisionARService VARService = null;
    private boolean mBound             = false;

    private Button writeImgBtn, vibrBtn, showIMUBtn;
    private Spinner imgSpinner, IMUSpinner;
    private NumberPicker vibrPicker;
    private SeekBar contrastSeekbar, brightnessSeekbar;
    private TextView infoPanel;

    private String[] vibrPickerValues;
    private String[] spinnerArrayImageName;
    private String[] spinnerArrayImage;

    /** Defines callbacks for service binding, passed to bindService() */
    private final ServiceConnection binderConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder iBinder) {
            // We've bound to VisionARService, cast the IBinder and get VisionARService instance
            VisionARService.VARBinder binder = (VisionARService.VARBinder) iBinder;
            VARService = binder.getService();

            mBound = true;

            VARService.setTouchpadListener(ev -> {
                if (ev == VisionARService.touchpadEvent.SINGLE_TAP) {
                    Log.d("act list ", "Single Tap");
                    runOnUiThread(() -> {
                        if (VARService.WriteImg(BitmapFactory.decodeResource(getResources(), R.drawable.single_tap)) == VisionARService.writeStatus.SUCCESS) {
                            infoPanel.setText("Single Tap");
                        }
                    });
                } else if (ev == VisionARService.touchpadEvent.DOUBLE_TAP) {
                    Log.d("act list ", "Double Tap");
                    runOnUiThread(() -> {
                        if (VARService.WriteImg(BitmapFactory.decodeResource(getResources(), R.drawable.double_tap)) == VisionARService.writeStatus.SUCCESS) {
                            infoPanel.setText("Double Tap");
                        }
                    });
                } else if (ev == VisionARService.touchpadEvent.SWIPE_DOWN) {
                    Log.d("act list ", "Swipe Down");
                    runOnUiThread(() -> {
                        if (VARService.WriteImg(BitmapFactory.decodeResource(getResources(), R.drawable.swipe_down)) == VisionARService.writeStatus.SUCCESS) {
                            infoPanel.setText("Swipe Down");
                        }
                    });
                } else {  // VisionARService.touchpadEvent.SWIPE_UP
                    Log.d("act list ", "Swipe Up");
                    runOnUiThread(() -> {
                        if (VARService.WriteImg(BitmapFactory.decodeResource(getResources(), R.drawable.swipe_up)) == VisionARService.writeStatus.SUCCESS) {
                            infoPanel.setText("Swipe Up");
                        }
                    });
                }
            });

            VARService.StartTouchpad();

            VARService.setVisionARListener(new VisionARService.VisionARListener() {
                @Override
                public void onConnect() {
                    Log.d("VisionAR Listener ", "VISIONAR CONNECTED");
                }

                @Override
                public void onDisconnect() {
                    Log.d("VisionAR Listener ", "VISIONAR DISCONNECTED");
                }
            });
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        initData();
    }


    @Override
    protected void onStart() {
        super.onStart();

        Intent intent = new Intent(this, it.univet.visionar.visionarservice.VisionARService.class);
        bindService(intent, binderConnection, Context.BIND_AUTO_CREATE);
    }


    @Override
    protected void onStop() {
        super.onStop();

        if (mBound) VARService.StopTouchpad();

        mBound = false;
        unbindService(binderConnection);
    }


    private void initView() {
        writeImgBtn       = findViewById(R.id.btn_img);
        vibrBtn           = findViewById(R.id.btn_vibr);
        showIMUBtn        = findViewById(R.id.btn_show_imu);
        imgSpinner        = findViewById(R.id.spinner_selection_image);
        IMUSpinner        = findViewById(R.id.spinner_selection_imu);
        vibrPicker        = findViewById(R.id.pickerVibration);
        contrastSeekbar   = findViewById(R.id.contr);
        brightnessSeekbar = findViewById(R.id.bright);
        infoPanel         = findViewById(R.id.info_label);

        setOnClickListeners();
        setSeekBarChangeListeners();
    }


    private void initData() {
        populateImgSpinner();
        setupVibrPicker();
        populateIMUSpinner();
    }


    private void populateImgSpinner() {

        int imagesArrayLength = getResources().getIntArray(R.array.images).length;

        spinnerArrayImageName = new String[imagesArrayLength];
        spinnerArrayImage = new String[imagesArrayLength];

        for(int i = 0; i < imagesArrayLength; i++) {
            spinnerArrayImageName[i] = getResources().getStringArray(R.array.imageName)[i];
            spinnerArrayImage[i] = getResources().getStringArray(R.array.images)[i];
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, spinnerArrayImageName);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        imgSpinner.setAdapter(adapter);
    }


    private void populateIMUSpinner() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.imu,
                                                                             android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        IMUSpinner.setAdapter(adapter);
    }


    private void setupVibrPicker() {
        final int PICKER_MIN = 100, PICKER_MAX = 3100, PICKER_STEP = 100;

        vibrPickerValues = new String[PICKER_MAX / PICKER_MIN];
        for (int i = 0; i < vibrPickerValues.length; i++) {
            vibrPickerValues[i] = String.valueOf(PICKER_STEP + (i * PICKER_STEP));
        }

        vibrPicker.setDisplayedValues(vibrPickerValues);
        vibrPicker.setMaxValue(29);
        vibrPicker.setMinValue(0);
    }


    private Bitmap getImgSpinnerBmp() {
        final int imgID = getResources().getIdentifier(spinnerArrayImage[imgSpinner.getSelectedItemPosition()], "drawable", getPackageName());

        if (imgID != 0) return BitmapFactory.decodeResource(getResources(), imgID);

        return BitmapFactory.decodeResource(getResources(), R.drawable.first_image_visionar);
    }


    private void setOnClickListeners() {
        writeImgBtn.setOnClickListener(v -> {
            v.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY, HapticFeedbackConstants.FLAG_IGNORE_GLOBAL_SETTING);

            if (VARService.WriteImg(getImgSpinnerBmp(), VisionARService.includeBattery.PLAIN) == VisionARService.writeStatus.SUCCESS) {
                Log.d("img ", "write image SUCCESS");
                infoPanel.setText("Write Image: SUCCESS\n" + spinnerArrayImageName[imgSpinner.getSelectedItemPosition()]);
            } else {
                Log.d("img ", "write image FAILED");
                infoPanel.setText("Write Image: FAILED\n");
            }
        });

        vibrBtn.setOnClickListener(v -> {
            v.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY, HapticFeedbackConstants.FLAG_IGNORE_GLOBAL_SETTING);
            int valueVibr = Integer.parseInt(vibrPickerValues[vibrPicker.getValue()]);

            if (VARService.Haptic(valueVibr) == VisionARService.writeStatus.SUCCESS) {
                Log.d("vibr ", "Haptic SUCCESS");
                infoPanel.setText("Haptic feedback: SUCCESS\n" + valueVibr + "[ms]");
            } else {
                Log.d("vibr ", "Haptic FAILED");
                infoPanel.setText("Haptic feedback: FAILED\n");
            }

        });

        showIMUBtn.setOnClickListener(v -> {
            v.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY, HapticFeedbackConstants.FLAG_IGNORE_GLOBAL_SETTING);

            float[] values = new float[3];

            switch (IMUSpinner.getSelectedItem().toString()) {
                case "Accelerometer":
                    if (VARService.GetIMUAcc(values) == VisionARService.writeStatus.SUCCESS) {
                        String txt = "Accelerometer data reading: SUCCESS" +
                                "\nAccelerometer X: " + values[0] + "[m/s^2]" +
                                "\nAccelerometer Y: " + values[1] + "[m/s^2]" +
                                "\nAccelerometer Z: " + values[2] + "[m/s^2]";

                        infoPanel.setText(txt);
                        writeImuDataOnGlasses(txt);
                    }

                    break;

                case "Gyroscope":
                    if (VARService.GetIMUGyro(values) == VisionARService.writeStatus.SUCCESS) {
                        String txt = "Gyroscope data reading: SUCCESS" +
                                "\nGyroscope X: " + values[0] + "[dps]" +
                                "\nGyroscope Y: " + values[1] + "[dps]" +
                                "\nGyroscope Z: " + values[2] + "[dps]";

                        infoPanel.setText(txt);
                        writeImuDataOnGlasses(txt);
                    }

                    break;

                case "Magnetometer":
                    if (VARService.GetIMUMag(values) == VisionARService.writeStatus.SUCCESS) {
                        String txt = "Magnetometer data reading: SUCCESS" +
                                "\nMagnetometer X: " + values[0] + "[G]" +
                                "\nMagnetometer Y: " + values[1] + "[G]" +
                                "\nMagnetometer Z: " + values[2] + "[G]";

                        infoPanel.setText(txt);
                        writeImuDataOnGlasses(txt);
                    }

                    break;

                default:
                    Toast.makeText(getBaseContext(), "Invalid IMU selection", Toast.LENGTH_LONG).show();
                    infoPanel.setText("IMU data reading: FAILED");
                    break;
            }
        });
    }


    private void setSeekBarChangeListeners() {
        contrastSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int contrastProgress = 120;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                contrastProgress = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (VARService.Contrast(contrastProgress) == VisionARService.writeStatus.SUCCESS) {
                    Log.d("contr ", "Contrast SUCCESS");
                    infoPanel.setText("Contrast setting: SUCCESS\nValue: " + contrastProgress);
                } else {
                    Log.d("contr ", "Contrast FAILED");
                    infoPanel.setText("Contrast setting: FAILED");
                }
            }
        });

        brightnessSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int brightnessProgress = 120;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                brightnessProgress = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (VARService.Brightness(brightnessProgress) == VisionARService.writeStatus.SUCCESS) {
                    Log.d("bright ", "Brightness SUCCESS");
                    infoPanel.setText("Brightness setting: SUCCESS\nValue: " + brightnessProgress);
                } else {
                    Log.d("bright ", "Brightness FAILED");
                    infoPanel.setText("Brightness setting: FAILED");
                }
            }
        });
    }
    private void writeImuDataOnGlasses(String txt) {
        if (VARService.WriteText(txt, VisionARService.includeBattery.INCLUDE) == VisionARService.writeStatus.SUCCESS) {
            Log.d("IMU data ", txt);
        } else {
            Log.d("IMU data ", "Reading FAILED");
        }
    }
}
