package it.univet.visionar.helloworld;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import it.univet.visionar.visionarservice.VisionARService;

public class SplashActivity extends AppCompatActivity implements View.OnClickListener {

    VisionARService VARService = null;
    boolean mBound             = false;

    /** Defines callbacks for service binding, passed to bindService() */
    private final ServiceConnection binderConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder iBinder) {
            // We've bound to VisionARService, cast the IBinder and get VisionARService instance
            VisionARService.VARBinder binder = (VisionARService.VARBinder) iBinder;
            VARService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        openWelcomeDialogInit();

        initView();
    }

    private void openWelcomeDialogInit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.custom_intro_dialog, null);
        builder.setView(dialogView);

        builder.setCancelable(false);

        TextView textLink = dialogView.findViewById(R.id.txtLink);
        textLink.setText(Html.fromHtml("<a href=\"http://www.univetar.com\">www.univetar.com</a>"));

        builder.setPositiveButton("Try it!", (dialog, which) -> dialog.cancel());

        AlertDialog dialog = builder.create();
        dialog.show();
        textLink.setMovementMethod(LinkMovementMethod.getInstance());
    }


    private void initView() {
        Button btn_enter = findViewById(R.id.btn_press);
        btn_enter.setOnClickListener(this);

        TextView tvPoweredBy = findViewById(R.id.textPoweredBy);
        tvPoweredBy.setText(Html.fromHtml("Powered by Univet"+ "<br>Before you start, please read:<br>" + "<a href=\"https://www.univetsafety.com/privacy-policy/\">Terms and conditions</a>"));
        tvPoweredBy.setLinkTextColor(Color.BLACK);
        tvPoweredBy.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent intent = new Intent(this, it.univet.visionar.visionarservice.VisionARService.class);
        startService(intent);

        bindService(intent, binderConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mBound = false;
        unbindService(binderConnection);
    }

    @Override
    public void onClick(View view) {
        if (mBound) {
            Bitmap bmpWelcomeImage = BitmapFactory.decodeResource(getResources(), R.drawable.first_image_visionar);

            if (VARService.WriteImg(bmpWelcomeImage) == VisionARService.writeStatus.SUCCESS) {
                Animation animZoomIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in);
                findViewById(R.id.visionar_image).startAnimation(animZoomIn);
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            } else {
                Toast.makeText(getBaseContext(), "You must connect VisionAR to proceed", Toast.LENGTH_LONG).show();
            }
        }
    }
}
